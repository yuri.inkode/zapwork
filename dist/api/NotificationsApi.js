"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _Message = _interopRequireDefault(require("../model/Message"));

var _SendMessage200Response = _interopRequireDefault(require("../model/SendMessage200Response"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Notifications service.
* @module api/NotificationsApi
* @version 1.0.11
*/
var NotificationsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new NotificationsApi. 
  * @alias module:api/NotificationsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function NotificationsApi(apiClient) {
    _classCallCheck(this, NotificationsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Send a new message
   * Send a new message
   * @param {module:model/Message} message Send a new message
   * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SendMessage200Response} and HTTP response
   */


  _createClass(NotificationsApi, [{
    key: "sendMessageWithHttpInfo",
    value: function sendMessageWithHttpInfo(message) {
      var postBody = message; // verify the required parameter 'message' is set

      if (message === undefined || message === null) {
        throw new Error("Missing the required parameter 'message' when calling sendMessage");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = ['application/json', 'application/xml', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = _SendMessage200Response["default"];
      return this.apiClient.callApi('/notificacoes', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null);
    }
    /**
     * Send a new message
     * Send a new message
     * @param {module:model/Message} message Send a new message
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SendMessage200Response}
     */

  }, {
    key: "sendMessage",
    value: function sendMessage(message) {
      return this.sendMessageWithHttpInfo(message).then(function (response_and_data) {
        return response_and_data.data;
      });
    }
  }]);

  return NotificationsApi;
}();

exports["default"] = NotificationsApi;