"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _Error = _interopRequireDefault(require("./Error"));

var _Message = _interopRequireDefault(require("./Message"));

var _SuccessMessage = _interopRequireDefault(require("./SuccessMessage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The SendMessage200Response model module.
 * @module model/SendMessage200Response
 * @version 1.0.11
 */
var SendMessage200Response = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>SendMessage200Response</code>.
   * @alias module:model/SendMessage200Response
   * @implements module:model/SuccessMessage
   * @implements module:model/Error
   * @implements module:model/Message
   */
  function SendMessage200Response() {
    _classCallCheck(this, SendMessage200Response);

    _SuccessMessage["default"].initialize(this);

    _Error["default"].initialize(this);

    _Message["default"].initialize(this);

    SendMessage200Response.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(SendMessage200Response, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>SendMessage200Response</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/SendMessage200Response} obj Optional instance to populate.
     * @return {module:model/SendMessage200Response} The populated <code>SendMessage200Response</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new SendMessage200Response();

        _SuccessMessage["default"].constructFromObject(data, obj);

        _Error["default"].constructFromObject(data, obj);

        _Message["default"].constructFromObject(data, obj);

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'Number');
        }

        if (data.hasOwnProperty('content')) {
          obj['content'] = _ApiClient["default"].convertToType(data['content'], 'String');
        }

        if (data.hasOwnProperty('api_key')) {
          obj['api_key'] = _ApiClient["default"].convertToType(data['api_key'], 'String');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'Number');
        }

        if (data.hasOwnProperty('message')) {
          obj['message'] = _ApiClient["default"].convertToType(data['message'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }
      }

      return obj;
    }
  }]);

  return SendMessage200Response;
}();
/**
 * @member {Number} status
 */


SendMessage200Response.prototype['status'] = undefined;
/**
 * @member {String} content
 */

SendMessage200Response.prototype['content'] = undefined;
/**
 * @member {String} api_key
 */

SendMessage200Response.prototype['api_key'] = undefined;
/**
 * @member {Number} number
 */

SendMessage200Response.prototype['number'] = undefined;
/**
 * @member {String} message
 */

SendMessage200Response.prototype['message'] = undefined;
/**
 * Order Status
 * @member {module:model/SendMessage200Response.TypeEnum} type
 */

SendMessage200Response.prototype['type'] = undefined; // Implement SuccessMessage interface:

/**
 * @member {Number} status
 */

_SuccessMessage["default"].prototype['status'] = undefined;
/**
 * @member {String} content
 */

_SuccessMessage["default"].prototype['content'] = undefined; // Implement Error interface:

/**
 * @member {Number} status
 */

_Error["default"].prototype['status'] = undefined;
/**
 * @member {String} content
 */

_Error["default"].prototype['content'] = undefined; // Implement Message interface:

/**
 * @member {String} api_key
 */

_Message["default"].prototype['api_key'] = undefined;
/**
 * @member {Number} number
 */

_Message["default"].prototype['number'] = undefined;
/**
 * @member {String} message
 */

_Message["default"].prototype['message'] = undefined;
/**
 * Order Status
 * @member {module:model/Message.TypeEnum} type
 */

_Message["default"].prototype['type'] = undefined;
/**
 * Allowed values for the <code>type</code> property.
 * @enum {String}
 * @readonly
 */

SendMessage200Response['TypeEnum'] = {
  /**
   * value: "text"
   * @const
   */
  "text": "text",

  /**
   * value: "voice"
   * @const
   */
  "voice": "voice",

  /**
   * value: "file"
   * @const
   */
  "file": "file",

  /**
   * value: "image"
   * @const
   */
  "image": "image",

  /**
   * value: "buttons"
   * @const
   */
  "buttons": "buttons"
};
var _default = SendMessage200Response;
exports["default"] = _default;