# ZapWorkOpenApi30.Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Number** |  | [optional] 
**content** | **String** |  | [optional] 


