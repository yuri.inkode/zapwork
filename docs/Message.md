# ZapWorkOpenApi30.Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** |  | [optional] 
**number** | **Number** |  | [optional] 
**message** | **String** |  | [optional] 
**type** | **String** | Order Status | [optional] 



## Enum: TypeEnum


* `text` (value: `"text"`)

* `voice` (value: `"voice"`)

* `file` (value: `"file"`)

* `image` (value: `"image"`)

* `buttons` (value: `"buttons"`)




