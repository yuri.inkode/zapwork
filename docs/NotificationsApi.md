# ZapWorkOpenApi30.NotificationsApi

All URIs are relative to *https://app.atendewhats.com.br/painel*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendMessage**](NotificationsApi.md#sendMessage) | **POST** /notificacoes | Send a new message



## sendMessage

> SendMessage200Response sendMessage(message)

Send a new message

Send a new message

### Example

```javascript
import ZapWorkOpenApi30 from 'zap_work_open_api_3_0';

let apiInstance = new ZapWorkOpenApi30.NotificationsApi();
let message = new ZapWorkOpenApi30.Message(); // Message | Send a new message
apiInstance.sendMessage(message).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **message** | [**Message**](Message.md)| Send a new message | 

### Return type

[**SendMessage200Response**](SendMessage200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json, application/xml, application/x-www-form-urlencoded
- **Accept**: application/json

