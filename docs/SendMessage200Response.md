# ZapWorkOpenApi30.SendMessage200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Number** |  | [optional] 
**content** | **String** |  | [optional] 
**apiKey** | **String** |  | [optional] 
**number** | **Number** |  | [optional] 
**message** | **String** |  | [optional] 
**type** | **String** | Order Status | [optional] 



## Enum: TypeEnum


* `text` (value: `"text"`)

* `voice` (value: `"voice"`)

* `file` (value: `"file"`)

* `image` (value: `"image"`)

* `buttons` (value: `"buttons"`)




