# ZapWorkOpenApi30.SuccessMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Number** |  | [optional] 
**content** | **String** |  | [optional] 


