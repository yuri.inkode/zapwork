// var normalize = require('normalize-object');
// var validTypes = ['upper', 'lower', 'snake', 'pascal', 'camel', 'kebab', 'constant', 'title', 'capital', 'sentence'];

var superagent = require('superagent')

// exports = module.exports = function (options) {
//   if (!options) options = {}
//   if (options instanceof superagent.Request) return serializer({}, options)
//   return serializer.bind(null, options)
// }

// function attachVerboseErrors(options, req) {
//   req.on('error', function (err) {
//     if (!err.response || (options.filter && !options.filter(err.response))) return
//     var contentType = err.response.get('Content-Type')
//     if (contentType && !contentType.startsWith('text/')) {
//       err.message += '\n' + JSON.stringify(err.response.body, null, 2)
//     } else {
//       err.message += '\n' + err.response.text
//     }
//   })
// }
/**
 * Wrapps Superagent and returns a serialized response
 * @param  {Object} superagent 
 * @return {void}
 */
function serializer(req) {
  if (arguments.length < 1) {
    throw new Error("superagent-serializer: expects 1 param");
  }
  // if (validTypes.indexOf(type) === -1) {
  //   throw new Error("superagent-serializer: the passed type don't exist");
  // }

  // var Request = superagent;
  var end = req.end;

  req.end = function(cb) {
    return end.call(this, function(err, res) {
      if (typeof cb !== 'function') return;

      var serializedRes = {body: res.body, text: res.text};

      try {
        if(res.type === 'text/html'){
          serializedRes.body = JSON.parse(res.text);
        }
        // serializedRes = normalize(serializedRes, type);
      } catch (e) {
        if (typeof res !== 'undefined') {
          serializedRes.text = res.text;
        }
      }

      cb(err, serializedRes);
    });
  };
}


module.exports = serializer;