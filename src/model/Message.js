/**
 * ZapWork - OpenAPI 3.0
 * Send messages with ZapWork
 *
 * The version of the OpenAPI document: 1.0.11
 * Contact: suporte@multiplica.me
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The Message model module.
 * @module model/Message
 * @version 1.0.11
 */
class Message {
    /**
     * Constructs a new <code>Message</code>.
     * @alias module:model/Message
     */
    constructor() { 
        
        Message.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>Message</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Message} obj Optional instance to populate.
     * @return {module:model/Message} The populated <code>Message</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Message();

            if (data.hasOwnProperty('api_key')) {
                obj['api_key'] = ApiClient.convertToType(data['api_key'], 'String');
            }
            if (data.hasOwnProperty('number')) {
                obj['number'] = ApiClient.convertToType(data['number'], 'Number');
            }
            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} api_key
 */
Message.prototype['api_key'] = undefined;

/**
 * @member {Number} number
 */
Message.prototype['number'] = undefined;

/**
 * @member {String} message
 */
Message.prototype['message'] = undefined;

/**
 * Order Status
 * @member {module:model/Message.TypeEnum} type
 */
Message.prototype['type'] = undefined;





/**
 * Allowed values for the <code>type</code> property.
 * @enum {String}
 * @readonly
 */
Message['TypeEnum'] = {

    /**
     * value: "text"
     * @const
     */
    "text": "text",

    /**
     * value: "voice"
     * @const
     */
    "voice": "voice",

    /**
     * value: "file"
     * @const
     */
    "file": "file",

    /**
     * value: "image"
     * @const
     */
    "image": "image",

    /**
     * value: "buttons"
     * @const
     */
    "buttons": "buttons"
};



export default Message;

